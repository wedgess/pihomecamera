import socket
from datetime import datetime

from pyfcm import FCMNotification


class FbConstants(object):
    USERS_NODE = "users"
    USERS_TOKEN_NODE = "token"
    USERS_IMAGES_NODE = "images"
    USERS_IMAGES_TIMESTAMP_NODE = "timestamp"
    USERS_IMAGES_TITLE_NODE = "title"
    USERS_IMAGES_URL_NODE = "url"
    USERS_IMAGES_PATH_NODE = "path"
    USERS_BUZZER_NODE = "buzzer"
    USERS_SENSOR_NODE = "sensors"
    USERS_SENSOR_TEMP_NODE = "temperature"
    USERS_SENSOR_HUMIDIDTY_NODE = "humidity"
    USERS_PI_ONLINE_NODE = "online"
    USERS_PI_IP_NODE = "pi_ip"


class Firebase(object):

    def __init__(self, fb):
        self.firebase = fb
        self.uuid = None
        self.token = None
        self.registration_id = None

    def add_user_image(self, image_name, image_url, local_img_path):
        db = self.firebase.database()
        now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        storage = self.firebase.storage()
        fireb_upload = storage.child(local_img_path).put(local_img_path, self.token)
        url = storage.child(local_img_path).get_url(fireb_upload['downloadTokens'])
        image_data = {FbConstants.USERS_IMAGES_TIMESTAMP_NODE: now, FbConstants.USERS_IMAGES_TITLE_NODE: image_name,
                      FbConstants.USERS_IMAGES_URL_NODE: url, FbConstants.USERS_IMAGES_PATH_NODE: local_img_path}
        id = db.child(FbConstants.USERS_NODE).child(self.uuid).child(FbConstants.USERS_IMAGES_NODE).push(image_data)
        print url
        id['img_url'] = url
        return id

    def update_sensor_data(self, temperature, humidity):
        db = self.firebase.database()
        print "[INFO] Uploading sensor data..."
        sensor_data = {FbConstants.USERS_SENSOR_TEMP_NODE: temperature,
                       FbConstants.USERS_SENSOR_HUMIDIDTY_NODE: humidity}
        db.child(FbConstants.USERS_NODE).child(self.uuid).child(FbConstants.USERS_SENSOR_NODE).set(sensor_data,
                                                                                                   self.token)

    def update_online_status(self, online):
        db = self.firebase.database()
        print "[INFO] Updating PI status..."
        db.child(FbConstants.USERS_NODE).child(self.uuid).child(FbConstants.USERS_PI_ONLINE_NODE).set(online,
                                                                                                      self.token)

    def set_buzzer_as_off(self):
        db = self.firebase.database()
        print "[INFO] Updating buzzer as inactive..."
        db.child(FbConstants.USERS_NODE).child(self.uuid).child(FbConstants.USERS_BUZZER_NODE).set(False, self.token)

    def get_stream(self, handler):
        db = self.firebase.database()
        my_stream = db.child(FbConstants.USERS_NODE).child(self.uuid).child(FbConstants.USERS_BUZZER_NODE).stream(
            handler)
        return my_stream

    def update_pi_ip(self):
        ip = \
            [l for l in
             ([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1],
              [[(s.connect(('8.8.8.8', 53)), s.getsockname()[0],
                 s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) if l][0][0]
        db = self.firebase.database()
        db.child(FbConstants.USERS_NODE).child(self.uuid).child(FbConstants.USERS_PI_IP_NODE).set(ip)
        print "[INFO] Pi IP address updated"

    def send_notification(self, img_url):
        # Send to single device.

        push_service = FCMNotification(
            api_key="AAAAl7dAKh8:APA91bHqxwh1xsZVfkXJu4TKjL-71q4Kt3LWZlTIZ_OQhgVmDVGzcskxzFBmNUnSmuoVH6QGfhWLs5xxDT1PZvhe245Bew4BVekVN69NwAWapVPAC3-Wki-yjPClI24qGMlaU28FUNGo")

        # Your api-key can be gotten from:  https://console.firebase.google.com/project/<project-name>/settings/cloudmessaging
        message_title = "Motion detected"
        message_body = img_url
        result = push_service.notify_single_device(registration_id=self.registration_id, message_title=message_title,
                                                   message_body=message_body)

        print result

    def send_data_notification(self, id, img_name, img_url, local_img_path):
        push_service = FCMNotification(
            api_key="AAAAl7dAKh8:APA91bHqxwh1xsZVfkXJu4TKjL-71q4Kt3LWZlTIZ_OQhgVmDVGzcskxzFBmNUnSmuoVH6QGfhWLs5xxDT1PZvhe245Bew4BVekVN69NwAWapVPAC3-Wki-yjPClI24qGMlaU28FUNGo")

        # Sending a notification with data message payload, keys must have _ at the end as they are parsed on Android to not break firebase storage url
        now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        image_data = {"id": id, FbConstants.USERS_IMAGES_TIMESTAMP_NODE: now,
                      FbConstants.USERS_IMAGES_TITLE_NODE: img_name,
                      FbConstants.USERS_IMAGES_URL_NODE: img_url, FbConstants.USERS_IMAGES_PATH_NODE: local_img_path}

        # To a single device
        result = push_service.single_device_data_message(registration_id=self.registration_id, data_message=image_data)

        print result
