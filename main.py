#!/usr/bin/env python
# USAGE
# python main.py

# import the necessary packages
from __future__ import absolute_import

import argparse
import sys
import threading
import warnings

from flask import Flask, render_template, Response

from buzzer import Buzzer
from camera import VideoCamera
from sensor import Sensor

exit_signal = threading.Event()  # your global exit signal

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-e", "--email", required=True,
                help="You must provide the email that you registered with on the Android application.")
ap.add_argument("-p", "--password", required=True,
                help="You must provide the password that you registered with on the Android application.")
args = vars(ap.parse_args())

config = {
    "apiKey": "AIzaSyAISI7ikha-oD83nhk3VcXwumCIagO_T4g",
    "authDomain": "pisecuritycamera.firebaseapp.com",
    "databaseURL": "https://pisecuritycamera.firebaseio.com",
    "projectId": "pisecuritycamera",
    "storageBucket": "pisecuritycamera.appspot.com",
    "serviceAccount": "./ServiceAccountKey.json"
}
from firebase_ops import Firebase

print "[INFO] Starting up..."
# import pyrebase
from pyrebase_mod import initialize_app

firebase = initialize_app(config)
firebase_ops = Firebase(firebase)
# filter warnings, load the configuration and initialize the Dropbox
# client
warnings.filterwarnings("ignore")
client = None
# App Globals (do not edit)
app = Flask(__name__)

user = None

print '[INFO] Logging in to Firebase'
auth = firebase.auth()
try:
    # Log the user in to firebase - do this here cause the script shouldn;t run if login fails
    user = auth.sign_in_with_email_and_password(args['email'], args['password'])
except:
    sys.exit("[ERROR] Could not log in to FireBase, check your credentials")

# successfull login set objects firebase variables
firebase_ops.uuid = user['localId']
firebase_ops.token = user['idToken']

# get user registration token from FB DB
db = firebase.database()
from firebase_ops import FbConstants
registration_id = db.child(FbConstants.USERS_NODE).child(firebase_ops.uuid).child(FbConstants.USERS_TOKEN_NODE).get().val()
firebase_ops.registration_id = registration_id
print "[INFO] Login successful: {}".format(firebase_ops.uuid)

# update pi ip address for user
firebase_ops.update_pi_ip()
firebase_ops.update_online_status(True)

# initialize sensor data
sensor = Sensor(firebase_ops)
# begin uploading sensor data
print "[INFO] Starting sensors..."
try:
    sensor.upload_sensor_data(exit_signal)
except AttributeError:
    print("Sensor values exception!")

# initialize the camera and grab a reference to the raw camera capture
camera = VideoCamera(firebase_ops)

token_refresh_thread = None

# Firebase refresh token must be updated every hour, to ensure that the token doesn't expire
# update the token every 50 minutes (60 * 50)
def refresh_firebase_token(exit_signal):
    if not exit_signal.is_set():
        print("[INFO] Refreshing firebase token")
        # do something here ...
        global user
        # Need to refresh this every 50 minutes
        user = auth.refresh(user['refreshToken'])
        global firebase_ops
        # now we have a fresh token
        firebase_ops.token = user['idToken']
        # call upload_sensor_data again in 30 seconds
        global token_refresh_thread
        token_refresh_thread = threading.Timer(60 * 50, refresh_firebase_token, [exit_signal])
        token_refresh_thread.daemon = True  # need to set as daemon or CTRL + C won;t kill this thread
        token_refresh_thread.start()


try:
    refresh_firebase_token(exit_signal)
except AttributeError:
    print("Sensor values exception!")

buzzer = Buzzer(firebase_ops)
buzzer.start_buzzer_listener()


@app.route('/')
def index():
    return render_template('index.html')


def gen(camera):
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


@app.route('/video_feed')
def video_feed():
    try:
        return Response(gen(camera),
                        mimetype='multipart/x-mixed-replace; boundary=frame')
    except Exception:
        print("Video feed exception")


camera_thread = None


def main():
    try:
        global camera_thread
        # start camera in another thread so the main thread can run Flask API
        camera_thread = threading.Thread(target=camera.start_camera, args=([exit_signal]))
        # start calling f now and every 60 sec thereafter
        print "[INFO] Starting camera..."
        camera_thread.daemon = True  # need to set as daemon or CTRL + C won;t kill this thread
        camera_thread.start()
        # start flask
        print "[INFO] Starting flask..."
        app.run(host='0.0.0.0', debug=False, threaded=True)
    except KeyboardInterrupt:  # on keyboard interrupt...
        exit_signal.set()  # send signal to all listening threads
        sensor.thread.join()
        token_refresh_thread.join()
        camera_thread.join()
        print("[INFO] Signalling all threads to exit")
        print("[INFO] Signalling all threads JOIN")
    finally:
        firebase_ops.update_online_status(False)


if __name__ == '__main__':
    main()
