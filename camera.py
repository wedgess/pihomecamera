from __future__ import absolute_import

import datetime
import threading
import time

import cv2
import imutils
from picamera import PiCamera
from picamera.array import PiRGBArray

from localimage import LocalImage

'''
This class handles all the camera operations such as detecting motion and ruuning the live stream.
Any images captures are then sent to firebase storage and the url is stored in firebase database
under the images node for the current user.
'''


class VideoCamera(object):

    def __init__(self, firebase, motion_sensitivity=4, upload_wait_seconds=5, resolution=(640, 480), fps=16):
        self.camera = PiCamera()
        self.camera.resolution = resolution
        self.camera.framerate = fps
        self.rawCapture = PiRGBArray(self.camera, size=resolution)
        self.frame = None
        self.avg = None
        self.lastUploaded = datetime.datetime.now()
        self.motionCounter = 0
        self.firebase = firebase
        # motion frames is 6
        self.motion_sensitivity = motion_sensitivity
        self.upload_wait_seconds = upload_wait_seconds
        print "[INFO] Camera warming up..."
        time.sleep(2.5)

    def get_frame(self):
        ret, jpeg = cv2.imencode('.jpg', self.frame)
        return jpeg.tobytes()

    def should_send_notification(self, motion_detected, timestamp, frame):
        # check to see if the room is occupied
        if motion_detected:
            print "[INFO] Motion detected at: {}".format(timestamp)
            # check to see if enough time has passed between uploads
            # min upload seconds is 5
            if (timestamp - self.lastUploaded).seconds >= self.upload_wait_seconds:
                # increment the motion counter
                self.motionCounter += 1
                # check to see if the number of frames with consistent motion is high enough
                if self.motionCounter >= self.motion_sensitivity:
                    print "[INFO] Uploading image"
                    # write the image to file so that it is stored locally on the Pi
                    t = LocalImage()
                    cv2.imwrite(t.path, frame)
                    img_url = t.get_img_url()
                    print "{url}".format(url=img_url)
                    id = self.firebase.add_user_image(t.get_img_name(), img_url, t.path)
                    self.firebase.send_data_notification(id['name'], t.get_img_name(), id['img_url'], t.path)
                    # update the last uploaded timestamp and reset the motion
                    # counter
                    self.lastUploaded = timestamp
                    self.motionCounter = 0
        else:
            self.motionCounter = 0  # otherwise, the room is not occupied

    def start_camera(self, exit_signal):
        if not exit_signal.is_set():
            # capture frames from the camera
            for f in self.camera.capture_continuous(self.rawCapture, format=u"bgr", use_video_port=True):
                # grab the raw NumPy array representing the image and initialize the timestamp and motion text
                frame = f.array
                timestamp = datetime.datetime.now()
                text = "No Motion"

                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                gray = cv2.GaussianBlur(gray, (21, 21), 0)
                # if the average frame is None, initialize it
                if self.avg is None:
                    self.avg = gray.copy().astype(u"float")
                    self.rawCapture.truncate(0)
                    continue

                # accumulate the weighted average between the current frame and previous frames, then
                # compute the difference between the current frame and running average
                cv2.accumulateWeighted(gray, self.avg, 0.5)
                frameDelta = cv2.absdiff(gray, cv2.convertScaleAbs(self.avg))

                # threshold the delta image, dilate the thresholded image to fill
                # in holes, then find contours on thresholded image, 5 is delta threshold
                thresh = cv2.threshold(frameDelta, 5, 255,
                                       cv2.THRESH_BINARY)[1]
                thresh = cv2.dilate(thresh, None, iterations=2)
                cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                                        cv2.CHAIN_APPROX_SIMPLE)
                cnts = cnts[0] if imutils.is_cv2() else cnts[1]

                # loop over the contours
                for c in cnts:
                    # if the contour is too small, ignore it, min area is 5000
                    if cv2.contourArea(c) < 5000:
                        continue

                    # compute the bounding box for the contour, draw it on the frame,
                    # and update the text
                    (x, y, w, h) = cv2.boundingRect(c)
                    cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                    text = "Motion Detected"

                # draw the text and timestamp on the frame
                ts = timestamp.strftime("%A %d %B %Y %H:%M:%S")
                if text == "Motion Detected":
                    # color red
                    cv2.putText(frame, "Camera Status: {}".format(text), (10, 20),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1)
                else:
                    # color green
                    cv2.putText(frame, u"Camera Status: {}".format(text), (10, 20),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1)

                # add black background to timestamp text
                cv2.rectangle(frame, (0, frame.shape[0]), (210, frame.shape[0] - 25),
                              (0, 0, 0), -1)
                cv2.putText(frame, ts, (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX,
                            0.35, (255, 255, 255), 1)

                # clear the stream in preparation for the next frame
                self.frame = frame
                thread = threading.Thread(target=self.should_send_notification,
                                          args=([text == "Motion Detected", timestamp, self.frame]))
                thread.start()
                self.rawCapture.truncate(0)
