# import the necessary packages
import os
import uuid
import socket

'''
This class stores the image locally on the PI. This class is being used so I can switch between Firebase
and being stored locally. This class has a method get_img_url which returns the images full remote URL.
'''

class LocalImage:
    def __init__(self, basePath="images", ext=".jpg"):
        # construct the file path
        self.path = "{base_path}/{rand}{ext}".format(base_path=basePath,
                                                     rand=str(uuid.uuid4()), ext=ext)
        self.img_name = self.path.replace(basePath + "/", "").replace(ext, "")

    def cleanup(self):
        # remove the file
        os.remove(self.path)

    def get_img_url(self):
        ip = \
            [l for l in
             ([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1],
              [[(s.connect(('8.8.8.8', 53)), s.getsockname()[0],
                 s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) if l][0][0]
        return "http://{ip}/{img_path}".format(ip=ip, img_path=self.path)

    def get_img_name(self):
        return self.img_name
