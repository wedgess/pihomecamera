import threading

from pyrebase.pyrebase import Database
from pyrebase.pyrebase import Firebase
from pyrebase.pyrebase import Stream


'''
This is a workaround for pyrebase streaming. Set thread daemon to keyboard interrupt finishes app.
'''

class Streamr(Stream):
    def start(self):
        self.thread = threading.Thread(target=self.start_stream, name={"stream_thread"})
        self.thread.daemon = True
        self.thread.start()
        return self


class Databaser(Database):
    def stream(self, stream_handler, token=None, stream_id=None):
        request_ref = self.build_request_url(token)
        return Streamr(request_ref, stream_handler, self.build_headers, stream_id)


class Firebaser(Firebase):
    def database(self):
        return Databaser(self.credentials, self.api_key, self.database_url, self.requests)


def initialize_app(config):
    return Firebaser(config)
