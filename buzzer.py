import time  # import the time library

import RPi.GPIO as GPIO  # import the GPIO library

from buzzer_thread import BuzzerThread

'''
This class is for the Piezo Buzzer attached to the PI. It streams events from the users buzzer node
in firebase database. The buzzer runs in a seperate thread as to not block other operations performed
on the main thread. BuzzerThread is created so that the buzzer can stop.
'''


class Buzzer(object):
    def __init__(self, firebase_ops, pin=18, pitch=1000, duration=2):
        self.pin = pin
        self.pitch = pitch
        self.firebase_ops = firebase_ops
        self.stream = None
        self.stop = False
        self.thread = None
        self.init_buzzer()

    def init_buzzer(self):
        GPIO.setmode(GPIO.BCM)  # Use the Broadcom method for naming the GPIO pins
        GPIO.setwarnings(False)
        GPIO.setup(self.pin, GPIO.OUT)  # Set pin 18 as an output pin
        self.buzzer = GPIO.PWM(self.pin, 0.5)

    def start_buzzer(self):
        # start the buzzer thread, it needs a thread or it blocks everything on main thread
        self.thread = BuzzerThread(target=self.play_buzzer_sound, args=())
        self.thread.start()

    def stop_buzzer(self):
        # stop the buzzer thread
        if self.thread is not None:
            self.thread._stop.set()
        self.stop = True
        GPIO.cleanup()
        self.firebase_ops.set_buzzer_as_off()

    def play_buzzer_sound(self):
        self.init_buzzer()
        self.stop = False
        self.buzzer.start(80)
        for i in range(0, 10):
            self.buzzer.ChangeFrequency(200)
            time.sleep(1)
            self.buzzer.ChangeFrequency(500)
            time.sleep(1)
        self.firebase_ops.set_buzzer_as_off()

    def stream_handler(self, message):
        # handle stream changes listening to the users buzzer node
        # the boolean value is inside "data", depending on this value start or stop the buzzer
        if message["data"]:
            self.start_buzzer()
        else:
            self.stop_buzzer()

    def start_buzzer_listener(self):
        # start stream event from users buzzer node
        self.stream = self.firebase_ops.get_stream(self.stream_handler)

    def stop_buzzer_listener(self):
        if self.stream is not None:
            self.stream.close()
