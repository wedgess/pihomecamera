#!/usr/bin/env python
import threading

import Adafruit_DHT

'''
This class handles reading sensor values from the DHT22 temp-humidity sensor. This reads values from the
sensor every 30 seconds and uploads the values tot he users sensors node in firebase database.
'''


class Sensor(object):
    def __init__(self, fireabse, sensor=22, pin=4):
        self.firebase_op = fireabse
        self.sensor = sensor
        self.pin = pin
        self.thread = None

    def read_sensor_values(self):
        humidity, temperature = Adafruit_DHT.read_retry(self.sensor, self.pin)
        if humidity is not None and temperature is not None:
            temp = '{0:0.1f}'.format(temperature)
            hum = '{0:0.1f}'.format(humidity)
            return [temp, hum]

    def upload_sensor_data(self, upload_sensor_data_stop):
        data = self.read_sensor_values()
        self.firebase_op.update_sensor_data(data[0], data[1])
        if not upload_sensor_data_stop.is_set():
            # call upload_sensor_data again in 30 seconds
            self.thread = threading.Timer(30, self.upload_sensor_data, [upload_sensor_data_stop])
            self.thread.daemon = True  # need to set as daemon or CTRL + C won;t kill this thread
            self.thread.start()
